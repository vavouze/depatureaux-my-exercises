import {my_display_alpha} from '../day-1/exercise-1.js';
import * as assert from 'assert';
describe('Exo 1', function () {
    describe('my_display_alpha', function () {
        it('should return abcdefghijklmnopqrstuvwxyz', function () {
            assert.equal(my_display_alpha(),'abcdefghijklmnopqrstuvwxyz');
        });
    });
});