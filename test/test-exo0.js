import {my_sum} from '../day-1/exercise-0.js';
import * as assert from 'assert';
describe('Exo 0', function () {
    describe('My Sum', function () {
        it('should return 0 when args 1 are undefined', function () {
            assert.equal(my_sum(2),0);
        });
        it('should return 0 when args not numbers', function () {
            assert.equal(my_sum('2',3),0);
        });
        it('should return 5', function () {
            assert.equal(my_sum(2,3),5);
        });
    });
});