import {my_length_array} from '../day-1/exercise-6.js';
import * as assert from 'assert';
describe('Exo 6', function () {
    describe('my_length_array', function () {
        it('should return 6', function () {
            assert.equal(my_length_array([1,2,3,4,5,6]),6);
        });
    });
});