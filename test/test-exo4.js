import {my_size_alpha} from '../day-1/exercise-4.js';
import * as assert from 'assert';
describe('Exo 4', function () {
    describe('my_size_alpha', function () {
        it('should return 0', function () {
            assert.equal(my_size_alpha(78),0);
        });

        it('should return 3', function () {
            assert.equal(my_size_alpha('abc'),3);
        });
    });
});