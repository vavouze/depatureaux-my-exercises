import {my_array_alpha} from '../day-1/exercise-5.js';
import * as assert from 'assert';
describe('Exo 5', function () {
    describe('my_array_alpha', function () {
        it('should return []', function () {
            assert.deepEqual(my_array_alpha(78),[]);
        });

        it('should return [abc]', function () {
            assert.deepEqual(my_array_alpha('abc'),['a','b','c']);
        });
    });
});