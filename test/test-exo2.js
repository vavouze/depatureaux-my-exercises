import {my_display_alpha_reverse} from '../day-1/exercise-2.js';
import * as assert from 'assert';
describe('Exo 2', function () {
    describe('my_display_alpha_reverse', function () {
        it('should return zyxwvutsrqponmlkjihgfedcba', function () {
            assert.equal(my_display_alpha_reverse(),'zyxwvutsrqponmlkjihgfedcba');
        });
    });
});