import {my_alpha_number} from '../day-1/exercise-3.js';
import * as assert from 'assert';
describe('Exo 3', function () {
    describe('my_display_alpha_reverse', function () {
        it('should return string', function () {
            assert.strictEqual(my_alpha_number(3),'3');
        });
    });
});