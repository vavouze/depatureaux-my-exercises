export const my_is_posi_neg = (nbr) => {
    if (typeof (nbr) === "undefined" || nbr === null ||typeof nbr !== "number")
    {
        return "POSITIF";
    }
    if (nbr > 0){
        return 'POSITIF';
    }else if (nbr < 0){
        return 'NEGATIF';
    }else {
        return 'NEUTRAL';
    }
};