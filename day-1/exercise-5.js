export const my_array_alpha = (str) => {
    if (typeof str === "undefined" || typeof str !== "string"){
        return [];
    }
    var i = 0;
    var toreturn = []
    while (str[i]){
        toreturn[i] = str[i];
        i++;
    }
    return toreturn;
};