export const my_sum = (a,b) => {
    if (typeof (a) === "undefined" || typeof (b) === "undefined" || typeof (a) !== "number" || typeof (b) !== "number"){
        return 0;
    }
    return a + b;
};