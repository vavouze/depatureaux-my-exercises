export const my_size_alpha = (str) => {
    if (typeof str === "undefined" || typeof str !== "string"){
        return 0;
    }
    var i = 0;
    var comp = 0;
    while (str[i]){
        comp++;
        i++;
    }
    return comp;
};